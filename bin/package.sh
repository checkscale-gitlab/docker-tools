#!/usr/bin/env bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ansi/ansi"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ndd-log4b/ndd-log4b.sh"
# shellcheck disable=SC1090
source "${PROJECT_DIR}/lib/ndd-utils4b/ndd-utils4b.sh"

ndd::base::catch_more_errors_on

ndd::logger::set_stdout_level "INFO"



function main() {

  local archive_version
  local distribution_dir="${PROJECT_DIR}/dist"

  if [[ -z "${1+x}" ]]; then
    archive_version="latest"
  else
    archive_version="${1}"
  fi

  log info "Packaging distribution to '${distribution_dir}/ndd-docker-tools-${archive_version}.tar.gz'"

  rm -rf "${distribution_dir:?}/"

  # ---------- ndd-docker-tools

  mkdir -p "${distribution_dir}/ndd-docker-tools-${archive_version}/"

  cp -r "${PROJECT_DIR}/src/entrypoint" "${distribution_dir}/ndd-docker-tools-${archive_version}/"

  ln -s "ndd-docker-tools-${archive_version}" "${distribution_dir}/ndd-docker-tools"

  while IFS= read -r -d '' file_path; do

    sed -i -E 's/@NDD_DOCKER_TOOLS_VERSION@/'"${archive_version}"'/g' "${file_path}"

  done < <(find "${distribution_dir}" -type f -print0)

  # ---------- package

  tar cfz "${distribution_dir}/ndd-docker-tools-${archive_version}.tar.gz" -C "${distribution_dir}/" \
    "ndd-docker-tools-${archive_version}" "ndd-docker-tools"

  log info "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
  log info "┃ PACKAGE -- Success!                                                           "
  log info "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
}

function error_handler() {
    local error_code="$?"

    test $error_code == 0 && return;

    log error "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"
    log error "┃ PACKAGE -- Failed!                                                            "
    log error "┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━"

    exit 1
}

trap 'error_handler ${?}' ERR

main "$@"

exit 0
